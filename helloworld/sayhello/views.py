from django.shortcuts import render

from django.http import HttpRequest, HttpResponse

def index(request):
	username=(request.headers['user-agent']).split(' ')[0]
	return HttpResponse("Hello, "+str(username)+'!')
