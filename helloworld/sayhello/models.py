from django.db import models

# Create your models here.

class OS_user(models.Model): #название оси и как называется пользователь, который на ней работает
	os_name=models.CharField(max_length=30)
	nickname=models.CharField(max_length=30)
	def __str__(self):
		return self.nickname
